import React, {useEffect, useState} from 'react'
import axios from "axios";
import Card from "./Components/Card";
import {RotatingLines} from "react-loader-spinner";

interface IChuckNorrisJoke {
    id: string,
    value: string
}

const App = () => {
    const [data, setData] = useState<IChuckNorrisJoke[]>([])

    const fetchChuckNorrisJokes = async () => {
        const arr: IChuckNorrisJoke[] = []
        for (let i = 0; i < 10; i++) {
            await axios.get('https://api.chucknorris.io/jokes/random')
                .then((response) => {
                    arr.push(response.data)
                })
        }
        setData(arr)
    }

    useEffect(() => {
        fetchChuckNorrisJokes()
    }, [])

    return (
        <div className="App">
            <div className="container mx-auto max-w-5xl px-4">
                <h1 className="text-3xl font-black mt-10">Chuck Norris Jokes API React</h1>
                <div className="grid sm:grid-cols-3 grid-cols-1 gap-5 mt-10 mb-10">
                    {
                        data.length ? data.map((item: IChuckNorrisJoke) =>
                            <Card key={item.id} id={item.id} value={item.value}/>)
                        :
                        <div className="absolute left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2">
                            <RotatingLines width="70"/>
                        </div>
                    }
                </div>
            </div>
        </div>
    )
}

export default App
