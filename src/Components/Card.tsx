import React from 'react'
import chuck from '../img/chuck1.jpeg'
import chuck1 from '../img/chuck1.jpeg'
import chuck2 from '../img/chuck2.jpeg'
import chuck3 from '../img/chuck3.jpeg'
import chuck4 from '../img/chuck4.jpeg'
import chuck5 from '../img/chuck5.webp'
import PropTypes from "prop-types";

const Card = ({id = '', value = ''}) => {

    const images: string[] = [chuck, chuck1, chuck2, chuck3, chuck4, chuck5]

    return (
        <div className="rounded-2xl border-2 border-gray-100 shadow-lg">
            <div className="h-32 w-full">
                <img className="object-cover rounded-tl-xl rounded-tr-xl h-full w-full"
                     src={images[Math.floor(Math.random() * images.length)]} alt="chuck"/>
            </div>
            <div className="p-3">
                <h3 className="font-bold">Id : {id}</h3>
                <hr className="my-3"/>
                <p>{value}</p>
            </div>
        </div>
    )
}

Card.propTypes = {
    id: PropTypes.string,
    value: PropTypes.string
}

export default Card